const express = require("express")
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
const cors = require("cors")
//require dotenv to variables
require("dotenv").config()
// call to routes
const router = require("./routes/router")

// call to controllers
const update = require("./controllers/updateData")

mongoose.connect(process.env.MONGO_URI, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
})
const app = express()

// //Load new data per hour
update()
setInterval(() => update(), 3600000)

//Uses for the app
//Parse of body to read data
app.use(bodyParser.json())

app.use(cors())

app.use("/", router)

const server = app.listen(3000, () => {
	console.log("server active")
})

module.exports = { app, server }
